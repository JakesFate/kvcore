jQuery(function ($) {
  // ----- Hero Tabs ----- //
  $(
    '<div id="hero-tabs" class="container"><div class="row"><ul class="nav nav-tabs row"><li><a class="active" data-target="#buy">Find A Home</a></li><li><a data-target="#sell">Sell A Home</a></li></ul></div></div>'
  ).insertBefore(".front #horizontal-search");
  $(".front #horizontal-search").wrap(
    '<div class="tab-content"><div id="buy" class="tab-pane active"></div><div id="sell" class="tab-pane"><div class="container"><iframe id="valuationFrame" style="width:100%;" src="/sellembed.php" allowtransparency="true" frameBorder="0"></iframe></div></div></div></div>'
  );
  $("ul.hero").css({
    width:
      $(".front #horizontal-search").find("> .container > .row").width() + "px",
  });
  $(".front #horizontal-search .location-form-group").attr(
    "class",
    "form-group location-form-group col-xs-12 col-md-2 col-lg-4"
  );

  $(".nav-tabs a").click(function () {
    $(this).parent().siblings().find("a.active").removeClass("active");
    jQuery(this).tab("show");
  });

  $("#valuationFrame").load(function () {
    $("#valuationFrame").contents().find("body").removeAttr("style");
    $("#valuationFrame").contents().find(".btn-success").removeAttr("style");
    $("#valuationFrame")
      .contents()
      .find("head")
      .append(
        '<style type="text/css">body{background:none;padding:0;height:150px;overflow:hidden;}.container-fluid{padding:0;}</style>'
      );
    $("#valuationFrame")
      .contents()
      .find("head")
      .append(
        '<style type="text/css">#sellform div div{width:100% !important;}</style>'
      );
    $("#valuationFrame")
      .contents()
      .find("head")
      .append(
        '<style type="text/css">#sellques{margin-bottom:0; border:none; box-shadow:none; padding:1rem 20px 0.95rem</style>'
      );
    $("#valuationFrame")
      .contents()
      .find("head")
      .append(
        '<style type="text/css">.btn-success{width:100%;height:54px;border-radius:0;font-weight:700;line-height:29px;background:#0065a8;font-size:1.05em;padding:9px 20px;transition:background-color .15s linear;} .btn-success:hover{background-color: #00375c;}</style>'
      );
    $("#valuationFrame")
      .contents()
      .find("head")
      .append(
        '<style type="text/css">@media screen and (min-width: 784px){ .container-fluid{padding:0 2px !important;} #sellform div div:first-of-type{width:83.33333% !important;} #sellform div div:last-of-type{width:16.66667% !important;}}</style>'
      );
    $("#valuationFrame")
      .contents()
      .find("#sellques")
      .parent()
      .attr("style", "width:75%;float:left");
    $("#valuationFrame")
      .contents()
      .find("#sellques")
      .attr("style", "height:54px;border-radius:0");
  });

  // ----- Search Fix ----- //
  //Horizontal Search
  $("#location_search-tokenfield").removeAttr("style");
  $("#listing_search_form .form-group.col-xs-12.col-md-2.col-lg-2")
    .first()
    .remove();
  $("#home-bodycontent #horizontal-search #more-options").remove();
  $("#listing_search_form .form-group.col-xs-6.col-md-2.col-lg-1").remove();
  $("#home-bodycontent .cover #horizontal-search .location-form-group")
    .removeClass("col-lg-3")
    .addClass("col-lg-9");
  $(
    "#home-bodycontent .cover #horizontal-search .form-group button#qs-submit"
  ).html('<i class="fa fa-search"></i> Search');
  $("#home-bodycontent .cover #horizontal-search #more-options").html(
    'Advanced Search <i class="fa fa-arrow-right"></i>'
  );
  $("#home-bodycontent .cover #horizontal-search")
    .attr({ "data-aos": "fade-right", "data-aos-delay": "500" })
    .show();

  //----- Update Navigation -----//
  $(".nav-primary").html(
    ' \
			<li class="nav-item"> \
				<a class="nav-link" href="/">Home</a> \
			</li> \
			<li class="nav-item nav-item-parent"> \
				<a href="#" class="nav-link">Buying</a> \
				<ul class="sub-menu"> \
					<li><a href="/resources/buyer-resources">Buyer Resources</a></li> \
					<li><a href="/finance.php">Get Pre-Approved</a></li> \
					<li><a href="/index.php?advanced=1&display=&custombox=&leadid=0&types%5B%5D=1&types%5B%5D=2&types%5B%5D=31&beds=0&baths=0&min=0&max=100000000&minfootage=0&maxfootage=30000&minacres=0&maxacres=3000&yearbuilt=0&maxyearbuilt=2019&walkscore=0&keywords=&rtype=map&sortby=listings.price+DESC">Home Search</a></li> \
					<li><a href="/index.php?rtype=map">Our Listings</a></li> \
				</ul> \
			</li> \
			<li class="nav-item nav-item-parent"> \
				<a href="#" class="nav-link">Selling</a> \
				<ul class="sub-menu"> \
					<li><a href="/resources/seller-resources">Seller Resources</a></li> \
					<li><a href="/resources/preparing-your-home-for-sale">Preparing Your Home For Sale</a></li> \
					<li><a href="/sell.php">What\'s My Home Worth?</a></li> \
				</ul> \
			</li> \
			<li class="nav-item nav-item-parent"> \
				<a class="nav-link" data-toggle="modal" data-target="#neighborhoodModal">Neighborhoods</a> \
			</li> \
			<li class="nav-item nav-item-parent"> \
				<a href="#" class="nav-link">Resources</a> \
				<ul class="sub-menu"> \
					<li><a href="https://www.hearthhomesteam.com/finance.php">Payment Calculator</a></li> \
					<li><a href="/contact.php">Contact Us</a></li> \
				</ul> \
			</li> \
			<li class="nav-item nav-item-parent"><a href="#" class="nav-link">About</a> \
				<ul class="sub-menu"> \
					<li><a href="/agents.php">Agent Profile</a></li> \
					<li><a href="/blog.php?agentid=261598">Real Estate Blog</a></li>\
					<li><a href="/testimonials.php">Testimonials</a></li> \
					<li><a href="/contact.php">Contact</a></li> \
				</ul> \
			</li> \
			<li class="nav-item"> \
				<a class="nav-link" href="/contact.php">Contact</a> \
			</li> \
	'
  );

  $("body").append(
    '<div class="modal fade" id="neighborhoodModal"><div class="modal-dialog modal-full"> \
		<div class="neighbor modal-content"> \
			<div class="modal-header"> \
				<button type="button" class="d-block mx-auto btn btn-link" data-dismiss="modal"><img class="img-fluid mx-auto mt20" src="https://dtzulyujzhqiu.cloudfront.net/fathomrealtybentonville358/profiles/1584038914.png" alt="Fathom Realty Logo" style="display: block;"></button><button type="button" class="close" data-dismiss="modal">&times;</button> \
			</div> \
			<div class="modal-header-search py-5 px-md-5"> \
				<div class="container m-b-2"> \
					<h3 class="text-white">Area Search</h3> \
		            <form method="get" action="/index.php#rslt" id="modal-search" autocomplete="off"> \
		               <div class="d-flex input-group location-form-group"> \
		                   <span class="input-group-addon"><i class="fa fa-search"></i></span> \
		                   <input type="hidden" name="quick" value="1"> \
		                   <input type="text" id="quick_header_search" \
		                          placeholder="Search City, Neighborhood, or Zip Code" \
		                          class="location_search_tahead location_search_tahead_quick" \
		                          autocomplete="on"> \
		                   <div class="hidden" id="areahide2"></div> \
		                   <input name="types[]" type="hidden" value="1"> \
		                   <input name="types[]" type="hidden" value="2"> \
		                   <input name="beds" type="hidden" value="0"> \
		                   <input name="baths" type="hidden" value="0"> \
		                   <input name="min" type="hidden" value="150000"> \
		                   <input name="max" type="hidden" value="100000000"> \
		                   <input name="rtype" value="grid" type="hidden"> \
		               </div><!-- /.form-group --> \
		           </form> \
		        </div> \
		    </div> \
			<div class="modal-body container"> \
		        <div class="neighborhoodList"> \
	        		<div class="row m-b-2"> \
		        		<h3 class="h5 text-left text-uppercase font-weight-bold w-100">Local Communities</h3> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Arlington">Arlington</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Bellevue">Bellevue</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Bothell">Bothell</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Edmonds">Edmonds</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Everett">Everett</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Green_lake">Green lake</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Kenmore">Kenmore</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Kirkland">Kirkland</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Lake_Stevens">Lake Stevens</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Lynnwood">Lynnwood</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Mukilteo">Mukilteo</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Marysville">Marysville</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Shoreline">Shoreline</a> \
	        			<a class="col-xs-6 col-md-3" href="/areas/Seattle">Seattle</a> \
	        		</div> \
	        		<div class="row"> \
			        	<div class="col-xs-12 m-b-3"> \
			        		<div class="row m-b-2"> \
				        		<h3 class="h5 text-left text-uppercase font-weight-bold w-100">Popular Neighborhoods</h3> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Ballard">Ballard</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Beacon_Hill_Neighborhood">Beacon Hill</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Belltown">Belltown</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Brier">Brier</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Broadview">Broadview</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Brookside">Brookside</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Bryant">Bryant</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Canyon_Park">Canyon Park</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Cathcart">Cathcart</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Central_Seattle">Central Seattle</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Downtown">Downtown Seattle</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Eastlake">East Lake</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Forrest_Park">Forrest Park</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Fremont">Fremont</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Georgetown">Georgetown</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Greenwood">Greenwood</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Lake_Ballinger">Lake Ballinger</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Lake_City">Lake City</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Lake_Forrest_Park">Lake Forrest Park</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Lake_Serene">Lake Serene</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Madison_Park">Madison Park</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Magnolia">Magnolia/a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Mill_Creek">Mill Creek</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Mountlake_Terrace">Mountlake Terrace</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Northgate">Northgate</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/North_Seattle">North Seattle</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Queen_Anne">Queen Anne</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Wallingford">Wallingford</a> \
			        			<a class="col-xs-6 col-md-4 col-lg-3" href="/areas/Woodinville">Woodinville</a> \
								<a class="col-xs-6" href="/areas/Seattle">Search All Seattle Neighborhoods</a> \
			        		</div> \
			        	</div> \
		        	</div> \
        		</div> \
        	</div> \
        <div class="modal-footer justify-content-center"> \
        		<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> \
        </div> \
    </div></div></div>'
  );

  //Add preferred lender to empty finance page - Add later
  /*$("#finance-bodycontent .main-inner .content .container .content").append(
    '<div class="row listing-row"> <div class="listing-detail col-md-12"> <div class="row"> <div class="col-lg-6"> <div class="overview"> <h2 class="text-xs-center text-md-left">Guaranteed Rate</h2> <div class="listing-user-image lender"> <a style="background-image: url(https://kunversion-frontend-blog.s3.amazonaws.com/images/customarea-buycary.com-214213-8a03e725011b6941b8d9672b5dde0888cc54a7d0.jpg);"></a> </div><ul> <li><strong>Name</strong><span>Mark Roberts</span></li><li><strong>License</strong><span>NMLS# 664423</span></li><li><strong>Phone</strong><span>(919) 902-8278</span></li><li class="text-xs-center text-md-right p-t-1"> <a class="btn btn-primary" target="_blank" href="https://www.rate.com/loan-expert/markroberts?utm_source=JustinHemker&utm_medium=website&utm_term=markroberts&utm_content=text">Get Started</a> </li></ul> </div></div><div class="col-lg-6"> <h2 class="text-xs-center text-md-left">Ask A Mortgage Question</h2> <div id="lender-form" class="cognito"></div></div><div class="col-sm-12"> <div id="finance-about"> <h2 class="text-xs-center text-md-left">Interview Q&A</h2> <p><strong>How long have you been in the mortgage business and what inspired you to get into it?</strong><br>I have been in the business since 1995. I liked the idea of helping folks achieve their dreams.</p><p><strong>What advice would you give new or first time home buyers?</strong><br>Ask a lot of questions and be patient.</p><p><strong>What can borrowers do to ensure a smooth mortgage underwriting process?</strong><br>I suggest that they follow my lead. I want it to be smooth as much as they do!</p><p><strong>What provides you with the most satisfaction in your job as a Loan Officer?</strong><br>I truly enjoy first time home buyers. There is nothing as satisfying as helping them close on their first home. </p></div></div></div></div></div>'
  );
  var url = window.location.href;
  if (jQuery("#lender-form").length) {
    Cognito.load("forms", { id: "1", entry: { URL: url } });
  }*/

  //Update sidebar contact info
  $(".widget table.contact tbody").html(
    '<tr><th>Call/Text:</th><td><a href="tel:425-931-9439">(425) 931-9439</a></td></tr>\
		<tr><th>Messenger:</th><td><a href="m.me/jacobwailes">m.me/Jacobwailes</a></td></tr>\
		<tr><th>Email:</th><td><a href="mailto:Jacob@hearthhomesteam.com">Jacob@hearthhomesteam.com</a></td></tr>'
  );

  $("#more-filters-button").text("Search Options");

  //If in site editor disable aos slide in effect
  if (window.location.href.indexOf("?editkey=") > -1) {
    $("#aos-stylesheet").attr("disabled", "disabled");
  }

  //If not in site editor...

  if (!(window.location.href.indexOf("?editkey=") > -1)) {
    if ($("#home-bodycontent #custom-quick-search").length) {
      $("#quick-search-title").after(
        '<form method="get" action="/index.php#rslt" id="header-search" autocomplete="off"><div class="input-group location-form-group"> <input type="hidden" name="advanced" value="1"> <label for="quick_header_search" class="sr-only">Location</label> <input type="text" id="quick_header_search" placeholder="City, Area, Zip, MLS# , or Address" class="location_search_tahead location_search_tahead_quick" autocomplete="off" data-use_area_polygons=""> <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span> <a class="input-group-addon" href="/index.php?homesearchmore=1&rtype=map"> <span><i class="fa fa-cogs" aria-hidden="true"></i></span> </a><div class="hidden" id="areahide2"></div> <input name="types[]" type="hidden" value="1"> <input name="types[]" type="hidden" value="2"> <input name="beds" type="hidden" value="0"> <input name="baths" type="hidden" value="0"> <input name="min" type="hidden" value="0"> <input name="max" type="hidden" value="100000000"> <input name="rtype" value="grid" type="hidden"></div></form>'
      );
    }
    //If you have testimonials and custom testimonials section exists

    if (
      $("#testimonials-carousel").length &&
      $("#testimonials-wrapper").length
    ) {
      //Remove unnecessary markup

      $("#testimonials h1").remove();

      $("#testimonials .carousel-control").remove();

      //Clone testimonials and add them to custom section on home page

      $("#testimonials-wrapper .content").prepend($("#testimonials"));

      //Remove default Testimonials section from bottom on home page

      $(".header-transparent #testimonials-carousel").remove();
    } else {
      //If no testimonials, remove custom testimonial section on home page

      $("#testimonials-wrapper").remove();
    }

    $("#home-bodycontent #about").addClass(
      "skew skew-top skew-bottom background-white section-inverse p-y-3"
    );

    $("#home-bodycontent #about .container").attr("data-aos", "fade-up");

    $("#home-bodycontent #latest-blogs").addClass(
      "skew skew-top skew-bottom section-inherit"
    );

    $("#home-bodycontent #latest-blogs .col-lg-6:nth-child(even)").attr(
      "data-aos",
      "fade-right"
    );

    $("#home-bodycontent #latest-blogs .col-lg-6:nth-child(odd)").attr({
      "data-aos": "fade-left",
      "data-aos-delay": "300",
    });

    //Move SEO Area content to above area stats and featured property images
    $(".area-content")
      .addClass("listing-box")
      .prependTo(".inner-main-content > .container");

    //If you have solds and custom solds section exists

    if (
      $(".listings.container").length &&
      $(".custom-listings-wrapper").length
    ) {
      var hasCarousel = false;

      $(".listings.container").each(function (i) {
        if ($(this).find('.page-header h2:contains("My Listings")').length) {
          $hasCarousel = true;

          //$(this).removeClass('container');

          $(this).find(".page-header").remove();

          $(this).find(".owl-nav").remove();

          //Move solds to custom section on home page

          $("#sold-listings-wrapper .content").prepend(this);

          //Add custom owl override scripts to bottom of body

          var script = document.createElement("script");

          //script.type = "text/javascript";

          script.src = "./recent-sold-carousel.js";

          //script.innerHTML = 'jQuery(document).ready(function(b){var a=b("#sold-wrapper .listing-carousel");var c=a.data();var d=c["owl.carousel"].options;d.items=2;d.responsive={0:{items:1,stagePadding:0},768:{items:2},1200:{items:3}};d.stagePadding=60;d.loop=true;d.margin=10;a.trigger("refresh.owl.carousel");b("#next-sold").click(function(){a.trigger("next.owl.carousel")});b("#prev-sold").click(function(){a.trigger("prev.owl.carousel")})});';

          document.body.appendChild(script);

          return false;
        }
      }); //END EACH

      //If no sold carousel, remove custom section wrapper from homepage

      if (!$hasCarousel) {
        $(".custom-listings-wrapper").remove();
      }
    }
  }
});
